The program should read in commands from one of three files Default.ini, Extreme.ini, Quick.ini, Decompress.ini. From there, it will create a linked list of commands. An ignored command can either be ignored when building the list, or removed in a separate step. The list of commands is then passed to the testing phase. Each node in the array can store the command arguments and the results of the testing phase.

Is there a way to more closely bind the ignore flags to the command list?
I could process the text of the program argument and extract the command name, then when building the command list, ignore anything with that name.
Ignorance processing would have to be moved to after flag processing.

How should the file list be stored?
Linked list baby

There is a better way to do almost everything I'm doing, I just didn't think to look it all up. For example, there's a standard C library function that deletes files, no need to fork and exec.