# Command-Line Compression Tool
`Codename: Pillbug`  
[`Current Version: 19.1.0`](https://gitlab.com/NuclearEagleFox/pillbug/tags/v19.1.0)

A small executable that tests various compression methods and uses the method that results in the smallest size for the given file.

### Building & Running
```
./pillbug [OPTIONS] [FILENAME(S)]
```

### Status & Contribution
At this time, I consider this project active. However, I am developing it for my own personal use. Forks are therefore preferred to pull requests.
